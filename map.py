from dataclasses import dataclass
from typing import Any, Tuple, List


@dataclass
class MapCell:
    left: Any
    right: Any
    floor: Any
    def __mul__(self, other):
        return [MapCell(self.left, self.right, self.floor) for _ in range(other)]

    def __hash__(self):
        return hash(self.floor.filename)


class Map:
    def __init__(self, width: int, length: int, height: int = 1, filler: MapCell = MapCell(None, None, None)):
        self.__width = width
        self.__length = length
        self.__height = height
        self.__cells: List[MapCell] = filler * (width * length * height)

    def check_size(self, x, y, z):
        if x > self.width:
            raise IndexError(f'x-coordinate cannot be greater than map\'s width ({self.__width})' )
        if y > self.length:
            raise IndexError(f'y-coordinate cannot be greater than map\'s length ({self.__length})' )
        if z > self.height:
            raise IndexError(f'z-coordinate cannot be greater than map\'s height ({self.__height})' )

    def add_cell(self, x, y, z, cell: MapCell):
        self.check_size(x, y, z)
        idx = z * self.length * self.width + y * self.width + x
        self.__cells[idx] = cell

    @property
    def size(self):
        return self.__width, self.__length, self.__height

    @property
    def width(self):
        return self.__width

    @property
    def length(self):
        return self.__length

    @property
    def height(self):
        return self.__height

    def get_cell_at(self, xyz: Tuple[int, int, int]) -> MapCell:
        x, y, z = xyz
        self.check_size(x, y, z)
        idx = z * self.length * self.width + y * self.width + x
        print(idx)
        return self.__cells[idx]

    def get_layer(self, z):
        offset = z * self.width * self.length
        return list(((x, y, z), self.__cells[offset + self.width * y + x])
        for y in range(self.length) for x in range(self.width))

    def __iter__(self):
        for idx, cell in enumerate(self.__cells):
            z, xy = divmod(idx, self.width * self.length)
            y, x = divmod(xy, self.width)
            yield ((x, y, z), cell)