import unittest
from typing import Tuple

from map import Map, MapCell


class MyTestCase(unittest.TestCase):
    def test_something(self):
        map = Map(3, 2, 1)
        map.get_cell_at((2,1,0))

    def test_index(self):
        map = Map(3, 2, 1)
        map.add_cell(2,1,0, MapCell(1, 2, 3))
        self.assertEqual(map.get_cell_at((2,1,0)), MapCell(1, 2, 3))

    def test_iter(self):
        map = Map(3, 4, 5)
        coords = tuple()
        for coords, cell in map:
            pass
        self.assertEqual(coords, (2, 3, 4))


if __name__ == '__main__':
    unittest.main()
