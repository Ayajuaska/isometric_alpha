import unittest

from main import real_to_isometric


class TestCoordinatesTranslation(unittest.TestCase):
    def test_real_to_isometric(self):
        x, y = real_to_isometric(0, 0, 0)
        print(x, y)

        x, y = real_to_isometric(1, 1, 0)
        print(x, y)
        print(real_to_isometric(2, 2, 0))


if __name__ == '__main__':
    unittest.main()
