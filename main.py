from enum import Enum
from typing import Tuple, Union

import numpy as np
from PIL import Image

from map import Map, MapCell

SIZE = 1024
GRID_SIZE = 32
BLACK = 0, 0, 0, 255
RED = 255, 0, 0, 255
GREEN = 0, 255, 0, 255
BLUE = 0, 0, 255, 255
Number = Union[int, float]


class WALL_SIDE(Enum):
    LEFT = 0
    RIGHT = 1


def translate(x, y):
    new_x = SIZE / 2 - x
    new_y = y * GRID_SIZE + x / 2
    return new_x, new_y


def set_pixel(drw: Image.Image, x, y, pixel):
    if (0 < x < SIZE) and (0 < y < SIZE):
        x = int(x)
        y = int(y)
        drw.putpixel((x, y), pixel)


def real_to_isometric(x: Number, y: Number, z: Number) -> Tuple[Number, Number]:
    """
    This looks strange because I want to turn axes
    Original rotation matrix was taken from https://en.wikipedia.org/wiki/Isometric_projection
    and then it was transposed
    """
    source = np.array([
        [y], [-z], [x]
        # [x], [y], [z]
    ]) # * -1

    rotation_matrix = np.array([
        [np.sqrt(3), 0, -np.sqrt(3)],
        [1, 2, 1],
        [np.sqrt(2), -np.sqrt(2), np.sqrt(2)]
    ]) / np.sqrt(6)
    # rotation_matrix = rotation_matrix.transpose()
    rotation = rotation_matrix.dot(source)
    x, y, _ = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 0]
    ]).dot(rotation)
    return x, y

def draw_floor_tile(img: Image, tile: Union[Image.Image, None], x, y, z, transpose: bool = False):
    if tile is None:
        return
    if transpose:
        tile = tile.transpose(Image.TRANSPOSE)
    for xx in range(x * GRID_SIZE, (x + 1) * GRID_SIZE):
        for yy in range(y * GRID_SIZE, (y + 1) * GRID_SIZE):
            color = tile.getpixel((GRID_SIZE - yy % GRID_SIZE - 1, xx % GRID_SIZE))
            set_pixel(img, xx + z * 138, yy, color)
            x_x, y_y = real_to_isometric(xx, yy, z * GRID_SIZE)
            set_pixel(img, x_x + SIZE//2, y_y + SIZE//2, color)


def draw_wall(img: Image, tile: Union[Image.Image, None], x, y, z, side: WALL_SIDE = WALL_SIDE.LEFT):
    if tile is None:
        return
    x *= GRID_SIZE
    y *= GRID_SIZE
    z *= GRID_SIZE
    direction = x if side == WALL_SIDE.LEFT else y
    for xx in range(GRID_SIZE):
        for zz in range(GRID_SIZE):
            if side == WALL_SIDE.LEFT:
                color = tile.getpixel((GRID_SIZE - xx - 1, GRID_SIZE - zz - 1))
                x_x, y_y = real_to_isometric(xx + direction, y, zz + z)
            else:
                color = tile.getpixel((xx, GRID_SIZE - zz - 1))
                x_x, y_y = real_to_isometric(x, xx + direction, zz + z)

            set_pixel(img, x_x + SIZE//2, y_y + SIZE//2, color)


def main():
    img = Image.new('RGBA', (SIZE, SIZE), '#ffffff')
    x, _ = translate(0, 0)
    tile_a = Image.open('./tiles/wall_a.png')
    floor_a = Image.open('./tiles/floor_a.png')
    floor_b = Image.open('./tiles/floor_b.png')
    floor_b_corner = Image.open('./tiles/floor_b_corner.png')

    map = Map(4, 4, 2, MapCell(tile_a, floor_b, floor_a))
    map.get_cell_at((1, 2, 1)).floor = tile_a

    for coords,  cell in map.get_layer(1):
        cell.right, cell.left = cell.left, cell.right
        if coords[0] % 2 != coords[1] % 2:
            cell.floor = None
        else:
            cell.floor = floor_b_corner

    for coords, cell in map:
        draw_wall(img, cell.left, *coords)
        draw_wall(img, cell.right, coords[0], coords[1], coords[2], WALL_SIDE.RIGHT)
        draw_floor_tile(img, cell.floor, coords[0], coords[1], coords[2])


    # for coords, cell in map.get_layer(1):
    #     draw_wall(img, cell.left, *coords)
    #     draw_wall(img, cell.right, coords[0], coords[1], coords[2], WALL_SIDE.RIGHT)
    #     draw_floor_tile(img, cell.floor, coords[0], coords[1], coords[2])

    img.show()


def draw_axes(img):
    for x in range(0, 1000, 10):
        x = x // 10
        x, y = real_to_isometric(x, 0, 0)
        set_pixel(img, x + SIZE // 2, y + SIZE // 2, RED)
    for y in range(0, 1000, 10):
        y = y // 10
        x, y = real_to_isometric(0, y, 0)
        set_pixel(img, x + SIZE // 2, y + SIZE // 2, GREEN)
    COLOR = list(BLUE)
    for z in range(0, 1000, 10):
        z = z // 10
        x, y = real_to_isometric(0, 0, z)
        set_pixel(img, x + SIZE // 2, y + SIZE // 2, tuple(COLOR))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
